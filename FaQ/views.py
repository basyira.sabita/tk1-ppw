from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Ask

def index1(request):
    return render(request, 'index2.html')

def addQuestion(request):
    Parameter = False
    nama = ""
    question = Ask.objects.all()

    if request.method == 'POST':
        Parameter = True
        name = request.POST.get("nama")
        pertanyaan = request.POST.get("Pertanyaan")
        q = Ask.objects.create(nama = name, qoa = pertanyaan)
        q.save()
        return redirect("/tanya/FaQ")

    context = {
        'param' : Parameter,
        'answer' : question
    }
    return render(request, 'ques_answ.html', context)
