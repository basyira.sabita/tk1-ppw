from django import forms
from .models import BlogPost

class PostForm(forms.ModelForm):
    class Meta:
        model = BlogPost
        fields = ('title','writer','body')

        widgets = {
            "title":forms.TextInput(attrs={'class': 'form-control'}),
            "writer":forms.TextInput(attrs={'class': 'form-control'}),
            "body":forms.Textarea(attrs={'class': 'form-control'})
        }