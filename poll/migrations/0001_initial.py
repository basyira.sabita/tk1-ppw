# Generated by Django 3.1.1 on 2020-11-11 17:59

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Poll',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pertanyaan', models.TextField()),
                ('pilihan_satu', models.CharField(max_length=50)),
                ('pilihan_dua', models.CharField(max_length=50)),
                ('pilihan_tiga', models.CharField(max_length=50)),
                ('pilihan_satu_hitung', models.IntegerField(default=0)),
                ('pilihan_dua_hitung', models.IntegerField(default=0)),
                ('pilihan_tiga_hitung', models.IntegerField(default=0)),
            ],
        ),
    ]
