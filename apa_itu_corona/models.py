from django.db import models

# Create your models here.
class AngkaCorona(models.Model):
    terkonfirmasi = models.CharField('Terkonfirmasi', max_length=10)
    perubahan = models.CharField('Perubahan', max_length=10)
    kasusAktif = models.CharField('Kasus Aktif',max_length=10)
    persenAktif = models.CharField('Persen Aktif',max_length=10)
    sembuh = models.CharField('Sembuh', max_length=10)
    persenSembuh = models.CharField('Persen Sembuh', max_length=10)
    meninggal = models.CharField('Meninggal', max_length=10)
    persenMeninggal = models.CharField('Persen Meninggal', max_length=10)
    tanggal = models.CharField('Tanggal', max_length=20)