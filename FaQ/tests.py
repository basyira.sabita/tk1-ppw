from django.test import TestCase, tag
from django.urls import reverse, resolve
from .views import addQuestion, index1
from .models import Ask

class FaQTest(TestCase):
    def test_url_exist(self):
        response = self.client.get(reverse('FaQ:index2'))
        self.assertEqual(response.status_code, 200)
        #untuk cek url bisa atau belum

    def test_landing_page_func(self):
        found = resolve('/tanya/FaQ/')
        self.assertEqual(found.func, addQuestion)
        #untuk test views yang dipake

    def test_used_template(self):
        response = self.client.get(reverse('FaQ:index2'))
        self.assertTemplateUsed(response, 'ques_answ.html')

    def test_create_Ask_object(self):
        Ask.objects.create(
            nama = 'Lontong',
            qoa = 'apa itu covid?',
        )
        count = Ask.objects.all().count()
        self.assertEqual(count, 1)
#buat yg kedua
    def test_url_exist_faq(self):
        response = self.client.get(reverse('FaQ:index1'))
        self.assertEqual(response.status_code, 200)
        #untuk cek url bisa atau belum

    def test_landing_page_func_faq(self):
        found = resolve('/tanya/tanyajawab/')
        self.assertEqual(found.func, index1)
        #untuk test views yang dipake

    def test_used_template_faq(self):
        response = self.client.get(reverse('FaQ:index1'))
        self.assertTemplateUsed(response, 'index2.html')
    