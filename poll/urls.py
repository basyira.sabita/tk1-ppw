from os import name
from django.urls import path
from . import views

app_name = 'poll'

urlpatterns = [
    path('',views.poll, name='poll'),
    path('vote/<poll_id>/', views.vote, name='vote'),
    path('hasil/<poll_id>/', views.hasil, name='hasil'),
    path('membuatPoll/', views.buatpoll, name='buatpoll')
]