from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView
from .models import BlogPost
from .forms import PostForm


class BlogView(ListView):
    model = BlogPost
    template_name = "blog.html"

class BlogDetailView(DetailView):
    model = BlogPost
    template_name = "blog-detail.html"

class BlogAddView(CreateView):
    model = BlogPost
    form_class = PostForm
    template_name = 'blog-add.html'