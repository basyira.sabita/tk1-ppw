from django.test import TestCase, Client
from .models import BlogPost

class Tester(TestCase):
    def test_model_BlogPost(self):
        mat = BlogPost.objects.create(
            title = "test",
            writer = "test",
            body = "test",
            )
        counter = BlogPost.objects.all().count()
        self.assertEqual(counter,1)

    def test_url_BlogPost(self):
        response = Client().get('/blogpost/')
        self.assertEquals(response.status_code,200)


    def test_url_BlogPostAdd(self):
        response = Client().get('/blogpost/post/')
        self.assertEquals(response.status_code,200)


# Create your tests here.
