from django.http import request
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Poll
from .forms import pollForm

# Create your views here.
def poll (request):
    polling = Poll.objects.all()
    context = {'polling': polling}
    return render (request,'poll/poll.html',context)

def buatpoll(request):
    if request.method =='POST':
        pollform = pollForm(request.POST)
        if pollform.is_valid():
            data = pollform.cleaned_data
            newPoll = Poll()
            newPoll.pertanyaan = data['pertanyaan']
            newPoll.pilihan_satu = data['pilihan_satu']
            newPoll.pilihan_dua = data['pilihan_dua']
            newPoll.pilihan_tiga = data['pilihan_tiga']
            newPoll.save()
            return redirect('poll:poll')
    else:
        pollform = pollForm()
        context = {'pollform': pollform}
        return render (request, 'poll/buatPoll.html', context)

def hasil(request,poll_id):
    poll = Poll.objects.get(pk=poll_id)
    context = {'poll':poll}
    return render (request,'poll/hasil.html', context)

def vote(request, poll_id):
    poll = Poll.objects.get(pk=poll_id)

    if request.method == 'POST':
        pilihan_terpilih = request.POST['poll']
        if pilihan_terpilih == 'pilihan1':
            poll.pilihan_satu_hitung += 1
        elif pilihan_terpilih == 'pilihan2':
            poll.pilihan_dua_hitung += 1
        elif pilihan_terpilih == 'pilihan3':
            poll.pilihan_tiga_hitung += 1;
        else:
            return HttpResponse(400, 'Invalid form')

        poll.save()
        return redirect ('poll:hasil', poll.id)
    context = {'poll':poll}
    return render (request,'poll/vote.html', context)