from django.http import response
from django.test import TestCase
from django.test import client
from django.urls import resolve, reverse
from django.test import Client
from .models import Poll
from django.apps import apps
from .views import poll, buatpoll, hasil, vote
from .forms import pollForm
from .apps import PollConfig
# Create your tests here.

class ModelTest(TestCase):
    def setUp(self):
        self.poll = Poll.objects.create(
            pertanyaan="Mana yang paling berbahaya?",
            pilihan_satu="Keluar tanpa masker",
            pilihan_dua="Keluar pakai masker",
            pilihan_tiga="Keluar pakai selain masker",
            pilihan_satu_hitung=0,
            pilihan_dua_hitung=0,
            pilihan_tiga_hitung=0,
        )
    
    def test_model_created(self):
        self.assertEqual(Poll.objects.count(),1)

class FormTest(TestCase):
    def test_formpoll_is_valid(self):
        form_poll = pollForm(data={
            "pertanyaan": "Mana yang paling berbahaya?",
            "pilihan_satu":"Keluar tanpa masker",
            "pilihan_dua":"Keluar pakai masker",
            "pilihan_tiga":"Keluar pakai selain masker",
        })
        self.assertTrue(form_poll.is_valid())

    def test_formpoll_is_invalid(self):
        form_poll = pollForm(data={})
        self.assertFalse(form_poll.is_valid())

class UrlTest(TestCase):

    def test_poll_url_ada(self):
        response = Client().get('/poll/')
        self.assertEqual(response.status_code,200)

    def test_poll_pakai_poll_template(self):
        response = Client().get('/poll/')
        self.assertTemplateUsed(response, 'poll/poll.html')
    
    def test_poll_pakai_poll_func(self):
        found = resolve('/poll/')
        self.assertEqual(found.func,poll)
    
    def test_buatPoll_url_ada(self):
        response = Client().get('/poll/membuatPoll/')
        self.assertEqual(response.status_code,200)

    def test_buatPoll_pakai_buatPoll_template(self):
        response = Client().get('/poll/membuatPoll/')
        self.assertTemplateUsed(response, 'poll/buatPoll.html')
    
    def test_buatPoll_pakai_buatPoll_func(self):
        found = resolve('/poll/membuatPoll/')
        self.assertEqual(found.func,buatpoll)

    def test_vote_url_ada(self):
        pollObjek = Poll.objects.create(id=1)
        response = Client().get('/poll/vote/' + str(pollObjek.id) + '/')
        self.assertEqual(response.status_code,200)

    def test_vote_pakai_vote_template(self):
        pollObjek = Poll.objects.create(id=1)
        response = Client().get('/poll/vote/' + str(pollObjek.id) + '/')
        self.assertTemplateUsed(response, 'poll/vote.html')
    
    def test_vote_pakai_vote_func(self):
        pollObjek = Poll.objects.create(id=1)
        found = resolve('/poll/vote/1/')
        self.assertEqual(found.func,vote)

    def test_hasil_url_ada(self):
        pollObjek = Poll.objects.create(id=1)
        response = Client().get('/poll/hasil/' + str(pollObjek.id) + '/')
        self.assertEqual(response.status_code,200)

    def test_hasil_pakai_hasil_template(self):
        pollObjek = Poll.objects.create(id=1)
        response = Client().get('/poll/hasil/' + str(pollObjek.id) + '/')
        self.assertTemplateUsed(response, 'poll/hasil.html')
    
    def test_hasil_pakai_hasil_func(self):
        pollObjek = Poll.objects.create(id=1)
        found = resolve('/poll/hasil/1/')
        self.assertEqual(found.func,hasil)

class ViewsTest(TestCase):
    def test_POST_buatPoll(self):
        response = self.client.post('/poll/membuatPoll/',{
            "pertanyaan": "Mana yang paling berbahaya?",
            "pilihan_satu":"Keluar tanpa masker",
            "pilihan_dua":"Keluar pakai masker",
            "pilihan_tiga":"Keluar pakai selain masker",
        }, follow=True)
        self.assertEqual(response.status_code,200)

    
class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(PollConfig.name, 'poll')
        self.assertEqual(apps.get_app_config('poll').name, 'poll')



    


