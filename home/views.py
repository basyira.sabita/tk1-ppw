from django.shortcuts import render, redirect
from .models import AngkaCorona
from .forms import FormAngkaCorona
# Create your views here.

def index(request):
    dataAngka = AngkaCorona.objects.all()
    return render(request, 'home/index.html', {"dataAngka":dataAngka})

def formAngkaCorona(request):
    form = FormAngkaCorona(request.POST or None)
    if form.is_valid() and request.method == "POST" :
        form.save()
        return redirect('/')
    else :
        return render(request, 'formAngka.html', {'form':form})