from django.db import models
from django.urls import reverse

class BlogPost(models.Model):
    title = models.CharField(max_length = 50)
    writer = models.CharField(max_length = 50)
    body = models.TextField()

    def __str__(self):
        return self.title + " | " + str(self.writer)

    def get_absolute_url(self):
        return reverse('blog-detail',args=(str(self.id)))