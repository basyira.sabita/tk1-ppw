from django.urls import path
from apa_itu_corona.views import apaItuCorona, formAngkaCorona

urlpatterns = [
    path('', apaItuCorona, name="apaItuCorona"),
    path('edit-form-angka-corona', formAngkaCorona),
]