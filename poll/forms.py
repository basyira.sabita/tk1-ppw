from django import forms

class pollForm(forms.Form):
    pertanyaan = forms.CharField(
        label= "Masukan Pertanyaan ",
        max_length=52,
        widget=forms.TextInput(
            attrs={
                'class':'form-control form-pertanyaan',
                'row' : '5',
                'null':'True'
            }  
        )
    )

    pilihan_satu = forms.CharField(
        label= "Pilihan 1 ",
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
            }  
        )
    )

    pilihan_dua = forms.CharField(
        label= "Pilihan 2 ",
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
            }  
        )
    )

    pilihan_tiga = forms.CharField(
        label= "Pilihan 3 ",
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
            }  
        )
    )

    def __init__(self, *args, **kwargs):
        super(pollForm, self).__init__(*args, **kwargs) # Call to ModelForm constructor
        self.fields['pertanyaan'].widget.attrs['style'] = 'box-sizing: border-box; border-radius: 20px; box-shadow:3px 3px 3px #cad2c5 ;'
        self.fields['pilihan_satu'].widget.attrs['style']  = 'box-sizing: border-box; border-radius: 20px; box-shadow:3px 3px 3px #cad2c5 '
        self.fields['pilihan_dua'].widget.attrs['style'] = 'box-sizing: border-box; border-radius: 20px; box-shadow:3px 3px 3px #cad2c5 '
        self.fields['pilihan_tiga'].widget.attrs['style'] = 'box-sizing: border-box; border-radius: 20px; box-shadow:3px 3px 3px #cad2c5'