from django.test import TestCase, Client
from .models import AngkaCorona
from .views import apaItuCorona, formAngkaCorona

# Create your tests here.
def test_url_apa_itu_corona(self):
    response = Client().get('/apa-itu-corona/')
    self.assertEquals(200, response.status_code)

def test_url_edit_form_apa_itu_corona(self):
    response = Client().get("/apa-itu-corona/edit-form-angka-corona/")
    self.assertEquals(200, response.status_code)

def test_views_apa_itu_corona(self):
    response = Client().get('/apa-itu-corona/')
    html = response.content.decode('utf-8')
    self.assertIn("ANGKA PERSEBARAN COVID-19", html)
    self.assertIn("TERKONFIRMASI", html)
    self.assertIn("KASUS AKTIF", html)
    self.assertIn("SEMBUH", html)
    self.assertIn("MENINGGAL", html)
    self.assertIn("COVID-19?", html)
    self.assertIn("Covid-19 adalah penyakit yang disebabkan oleh virus Corona. Kasus Covid-19 pertama kali ditemukan di Wuhan, Cina pada Desember 2019. Covid-19 didiagnosa dengan uji lab. Gejala dari Covid-19 muncul 14 hari sejak kontak dengan virus Corona.", html)
    self.assertIn("GEJALA COVID-19", html)
    self.assertIn("CARA PENYEBARAN", html)
    self.assertIn("CARA PENGOBATAN", html)
    self.assertIn("Covid-19 disebarkan melalui droplet yang ada di udara dari batuk atau bersin orang yang terjangkit. Secara umum, droplet tidak bertahan lama di udara karena akan jatuh ke tanah atau ke suatu permukaan. Maka, dari itu physical distancing efektif sebagai tindakan preventif.", html)
    self.assertIn("Hingga saat ini, belum ada obat khusus yang digunakan untuk mencegah ataupun mengobati penyakit Covid-19. Pasien terjangkit Covid-19 ditangani hanya untuk meredakan dan mengobati gejala. Untuk kasus yang serius harus dibawa ke rumah sakit.", html)
    self.assertIn('<img src="{% static "images/virusCorona.png" %}" alt="Virus Corona" width="282px">', html)

def test_views_edit_form(self):
    response = Client().get('/apa-itu-corona/edit-form-angka-corona/')
    html = response.content.decode('utf-8')
    self.assertIn('<input type="submit" value="Submit" class="btn btn-primary click">', html)

def test_template_apa_itu_corona(self):
    response = Client().get('/apa-itu-corona/')
    self.assertTemplateUsed(response, 'apa_itu_corona.html')

def test_template_formAngka(self):
    response = Client().get('/apa-itu-corona/edit-form-angka-corona/')
    self.assertTemplateUsed(response, 'formAngka.html')

def test_model_form_kegiatan(self):
    AngkaCorona.objects.create(kegiatan="Ngoding")
    jumlah = AngkaCorona.objects.all().count()
    self.assertEquals(jumlah, 1)